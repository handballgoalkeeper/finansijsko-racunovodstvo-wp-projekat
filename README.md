# Wp Projekat Finansijsko Racunovodstvo

## Name
Finansijsko računovodstvo

## Description
Web aplikacija namenjena za koriscenje u knjigovodstvene svrhe.

# Functions

1. Podrzava rad sa kontnim okvirom, uvoz kontnog okvira iz .xlsx fajla...
2. Podrzava rad sa nalozima kao sto su kreiranje novog naloga, dodavanje i pregled stavki naloga, knjizenje naloga...
3. Podrzava rad sa komitentima kao sto su kreiranje, pregled, pretraga, izmena...
4. Podrzava autorizaciju, kao i dodavanje i brisanje korisnika, i njegovih pristupnih prava.
5. Sadrzi izveštaje kao što su **Bruto bilans**, **Glavna knjiga**, **Dnevnik**

## Authors
Student: ***Dimitrije Jelić***

## Other
Profesor: ***Đorđe Obradović***<br>
Asistent: ***Ivan Radosavljević***

