import base64
import flask
from flask import Flask
from flaskext.mysql import MySQL
import pymysql
import time

app = Flask(__name__, static_folder="static", static_url_path="/")
mysql = MySQL(app, cursorclass=pymysql.cursors.DictCursor)

app.config["MYSQL_DATABASE_USER"] = 'root'
app.config["MYSQL_DATABASE_PASSWORD"] = 'Kokorini34492'
app.config["MYSQL_DATABASE_DB"] = 'finansijsko_racunovodstvo'
app.secret_key = "ovo je neki tajni kljuc"

# Login / Logout


@app.route("/")
def login():
    if flask.session.get("korisnik") is not None:
        return flask.redirect("/home")
    return app.send_static_file("login.html")


@app.route("/logout")
def logout():
    flask.session.pop("korisnik", None)
    return flask.redirect("/")


@app.route("/api/login", methods=["POST"])
def login_check():
    username_input = flask.request.json['username']
    password_input = flask.request.json['password'].encode('UTF-8')
    encoded_password_input = base64.b64encode(password_input)
    cursor = mysql.get_db().cursor()
    cursor.execute(
        "SELECT * FROM korisnik WHERE username=%s AND password=%s", (username_input, encoded_password_input))
    korisnik = cursor.fetchone()

    if korisnik is None:
        return flask.jsonify(None), 403
    else:
        cursor.execute(
            "SELECT pravo_pristupa_id FROM dodeljeno_pravo WHERE korisnik_id=%s", (korisnik["id"], ))
        dodeljena_prava_pristupa = cursor.fetchall()
        prava_pristupa = list(
            map(lambda x: x["pravo_pristupa_id"], dodeljena_prava_pristupa))
        flask.session["korisnik"] = {
            "username": username_input,
            "prava_pristupa": prava_pristupa
        }
        return flask.jsonify(None), 200

# home


@app.route("/home")
def home():
    if flask.session.get("korisnik") is not None and len(set(flask.session.get("korisnik")["prava_pristupa"]).intersection({1})) > 0:
        return app.send_static_file("index.html")
    elif flask.session.get("korisnik") is not None and len(set(flask.session.get("korisnik")["prava_pristupa"]).intersection({2})) > 0:
        return app.send_static_file("index.html")
    elif flask.session.get("korisnik") is not None and len(set(flask.session.get("korisnik")["prava_pristupa"]).intersection({3})) > 0:
        return flask.redirect("/adminHome")
    return flask.redirect("/")


@app.route("/adminHome")
def adminHome():
    if flask.session.get("korisnik") is not None and len(set(flask.session.get("korisnik")["prava_pristupa"]).intersection({3})) > 0:
        return app.send_static_file("administrator/home.html")
    elif flask.session.get("korisnik") is not None and len(set(flask.session.get("korisnik")["prava_pristupa"]).intersection({1})) > 0:
        return flask.redirect("/home")
    elif flask.session.get("korisnik") is not None and len(set(flask.session.get("korisnik")["prava_pristupa"]).intersection({2})) > 0:
        return flask.redirect("/home")
    return flask.redirect("/")
# pregled kontnog okvira


@app.route("/kontni_okvir")
def kontni_okvir_render():
    return app.send_static_file("kontni_okvir/kontni_okvir.html")


@app.route("/kontni_okvir/uvoz")
def uvoz_kontnog_okvira_render():
    return app.send_static_file("kontni_okvir/uvoz_kontnog_okvira.html")


@app.route("/kontni_okvir/novi_konto")
def kreiranje_novog_konta():
    return app.send_static_file("kontni_okvir/novi_konto.html")


@app.route("/komitenti")
def komitenti_render():
    return app.send_static_file("komitenti/komitenti.html")


@app.route("/komitenti/novi_komitent")
def novi_komitent_render():
    return app.send_static_file("komitenti/novi_komitent.html")


@app.route("/nalozi")
def pregled_naloga_render():
    return app.send_static_file("nalozi/nalozi.html")


@app.route("/nalozi/novi_nalog")
def dodaj_nalog():
    return app.send_static_file("nalozi/novi_nalog.html")


@app.route("/dnevnik")
def dnevnik_render():
    return app.send_static_file("izvestaji/dnevnik.html")


@app.route("/izvestaji")
def izvestaji_render():
    return app.send_static_file("izvestaji/izvestaji_main.html")


@app.route("/bruto_bilans")
def bruto_bilans_render():
    return app.send_static_file("izvestaji/bruto_bilans.html")


@app.route("/glavna_knjiga")
def glavna_knjiga_render():
    return app.send_static_file("izvestaji/glavna_knjiga.html")


@app.route("/dodaj_korisnika")
def dodaj_korisnika():
    return app.send_static_file("administrator/dodaj_korisnika.html")


@app.route("/api/kontni_okvir", methods=["POST"])
def uvoz_kontnog_okvira():
    if len(set(flask.session.get("korisnik")["prava_pristupa"]).intersection({1})) > 0:
        promene = 0
        db = mysql.get_db()
        cursor = db.cursor()
        # if len(flask.request.json) > 1:
        #     cursor.execute("TRUNCATE TABLE kontna_shema")
        for k in flask.request.json:
            promene += cursor.execute(
                "INSERT INTO kontni_okvir(konto, naziv, tip) VALUES(%(konto)s, %(naziv)s, %(tip)s)", k)
        db.commit()
        if promene == 0:
            return flask.jsonify(None), 500
        return flask.jsonify(None), 201
    return "", 403


@app.route("/api/kontni_okvir")
def dobavi_kontni_okvir():
    cursor = mysql.get_db().cursor()
    cursor.execute("SELECT * FROM kontni_okvir")
    kontni_okvir = cursor.fetchall()
    for k in kontni_okvir:
        k["pocetno_stanje_duguje"] = 0 if k["pocetno_stanje_duguje"] == None else k["pocetno_stanje_duguje"]
        k["pocetno_stanje_potrazuje"] = 0 if k["pocetno_stanje_potrazuje"] == None else k["pocetno_stanje_potrazuje"]
        k["duguje"] = 0 if k["duguje"] == None else k["duguje"]
        k["potrazuje"] = 0 if k["potrazuje"] == None else k["potrazuje"]
        k["saldo"] = 0 if k["saldo"] == None else k["saldo"]
        k["promet_pocetno_duguje"] = k["pocetno_stanje_duguje"] + k["duguje"]
        k["promet_pocetno_potrazuje"] = k["pocetno_stanje_potrazuje"] + k["potrazuje"]
        if k["konto"] == "0220":
            k["dugovna_saldo"] = k["promet_pocetno_duguje"] - \
                k["promet_pocetno_potrazuje"]
            k["potrazuje_saldo"] = 0
        elif k["konto"] == "0229":
            k["dugovna_saldo"] = 0
            k["potrazuje_saldo"] = k["promet_pocetno_potrazuje"] - \
                k["promet_pocetno_duguje"]
        elif k["konto"] == "2040":
            k["dugovna_saldo"] = k["promet_pocetno_duguje"] - \
                k["promet_pocetno_potrazuje"]
            k["potrazuje_saldo"] = 0
        elif k["konto"] == "3010":
            k["dugovna_saldo"] = 0
            k["potrazuje_saldo"] = k["promet_pocetno_potrazuje"] - \
                k["promet_pocetno_duguje"]
        elif k["konto"] == "3400":
            k["dugovna_saldo"] = 0
            k["potrazuje_saldo"] = k["promet_pocetno_potrazuje"] - \
                k["promet_pocetno_duguje"]
        elif k["konto"] == "4350":
            k["dugovna_saldo"] = 0
            k["potrazuje_saldo"] = k["promet_pocetno_potrazuje"] - \
                k["promet_pocetno_duguje"]
        elif k["konto"] == "6140":
            k["dugovna_saldo"] = 0
            k["potrazuje_saldo"] = k["promet_pocetno_potrazuje"] - \
                k["promet_pocetno_duguje"]
        elif k["konto"] == "2410":
            k["dugovna_saldo"] = k["promet_pocetno_duguje"] - \
                k["promet_pocetno_potrazuje"]
            k["potrazuje_saldo"] = 0
        elif k["konto"] == "5511":
            k["dugovna_saldo"] = k["promet_pocetno_duguje"] - \
                k["promet_pocetno_potrazuje"]
            k["potrazuje_saldo"] = 0
        else:
            k["dugovna_saldo"] = k["promet_pocetno_duguje"] - \
                k["promet_pocetno_potrazuje"]
            k["potrazuje_saldo"] = k["promet_pocetno_potrazuje"] - \
                k["promet_pocetno_duguje"]
    if kontni_okvir is None:
        return flask.jsonify(kontni_okvir), 404
    return flask.jsonify(kontni_okvir), 200


@app.route("/api/kontni_okvir/<string:konto>", methods=["DELETE"])
def ukloni_konto(konto):
    if len(set(flask.session.get("korisnik")["prava_pristupa"]).intersection({1})) > 0:
        db = mysql.get_db()
        cursor = db.cursor()
        izmene = cursor.execute(
            "DELETE FROM kontni_okvir WHERE konto=%s", (str(konto), ))
        db.commit()
        if izmene == 0:
            return flask.jsonify(None), 404
        return flask.jsonify(None), 200
    return "", 403


@app.route("/api/kontni_okvir/<string:konto>", methods=["PUT"])
def izmeni_konto(konto):
    if len(set(flask.session.get("korisnik")["prava_pristupa"]).intersection({1})) > 0:
        db = mysql.get_db()
        cursor = db.cursor()
        izmene = cursor.execute(
            "UPDATE kontni_okvir SET naziv=%(naziv)s, tip=%(tip)s WHERE konto=%(konto)s", flask.request.json)
        db.commit()
        if izmene == 0:
            return flask.jsonify(None), 404
        return flask.jsonify(None), 200
    return "", 403


@app.route("/api/kontni_okvir/<string:konto>", methods=["GET"])
def dobavi_konto(konto):
    cursor = mysql.get_db().cursor()
    cursor.execute("SELECT * FROM kontni_okvir WHERE konto=%s", (konto, ))
    konto_detaljno = cursor.fetchone()
    if konto_detaljno["duguje"] is None:
        konto_detaljno["duguje"] = 0
    if konto_detaljno["potrazuje"] is None:
        konto_detaljno["potrazuje"] = 0
    if konto_detaljno["saldo"] is None:
        konto_detaljno["saldo"] = konto_detaljno["duguje"] - \
            konto_detaljno["potrazuje"]
    if konto_detaljno is None:
        return flask.jsonify(None), 404
    return flask.jsonify(konto_detaljno), 200


@app.route("/api/permisije")
def prava_pristupa():
    if flask.session["korisnik"] is None:
        return flask.jsonify(None), 403
    return flask.jsonify(flask.session["korisnik"]), 200


@app.route("/api/permisije/<int:id>")
def prava_pristupa_pojedinacno(id):
    cursor = mysql.get_db().cursor()
    cursor.execute(
        "SELECT pravo_pristupa_id FROM dodeljeno_pravo WHERE korisnik_id=%s", (id, ))
    dodeljena_prava_pristupa = cursor.fetchall()
    prava_pristupa = prava_pristupa = list(
        map(lambda x: x["pravo_pristupa_id"], dodeljena_prava_pristupa))
    return flask.jsonify(prava_pristupa), 200
# Komitenti


@app.route("/api/korisnici")
def dobavi_korisnike():
    cursor = mysql.get_db().cursor()
    cursor.execute("SELECT id, username FROM korisnik")
    korisnici = cursor.fetchall()
    if korisnici is None:
        return flask.jsonify(korisnici), 404
    return flask.jsonify(korisnici), 200


@app.route("/api/komitenti")
def dobavi_komitente():
    cursor = mysql.get_db().cursor()
    cursor.execute("SELECT * FROM komitent")
    komitenti = cursor.fetchall()
    if komitenti is None:
        return flask.jsonify(komitenti), 404
    return flask.jsonify(komitenti), 200


@app.route("/api/komitenti/<int:id>", methods=["DELETE"])
def obrisi_komitenta(id):
    if len(set(flask.session.get("korisnik")["prava_pristupa"]).intersection({1})) > 0:
        db = mysql.get_db()
        cursor = db.cursor()
        izmene = cursor.execute(
            "DELETE FROM komitent WHERE id=%s", (id, ))
        db.commit()
        if izmene == 0:
            return flask.jsonify(None), 404
        return flask.jsonify(None), 200
    return "", 403

# Pitaj majku da li korisnik moze dodati ili izmeniti komitenta


@app.route("/api/komitenti", methods=["POST"])
def kreiraj_novog_komitenta():
    if len(set(flask.session.get("korisnik")["prava_pristupa"]).intersection({1})) > 0:
        promene = 0
        db = mysql.get_db()
        cursor = db.cursor()
        for k in flask.request.json:
            promene += cursor.execute(
                "INSERT INTO komitent(naziv, adresa, maticni_broj, pib, tekuci_racun, opis) VALUES(%(naziv)s, %(adresa)s, %(maticni_broj)s, %(pib)s, %(tekuci_racun)s, %(opis)s)", k)
        db.commit()
        if promene == 0:
            return flask.jsonify(None), 500
        return flask.jsonify(None), 201
    return "", 403


@app.route("/api/komitenti/<int:id>", methods=["PUT"])
def izmeni_komitenta(id):
    if len(set(flask.session.get("korisnik")["prava_pristupa"]).intersection({1})) > 0:
        db = mysql.get_db()
        cursor = db.cursor()
        komitent = dict(flask.request.json)
        komitent["id"] = id
        izmene = cursor.execute(
            "UPDATE komitent SET naziv=%(naziv)s, adresa=%(adresa)s, maticni_broj=%(maticni_broj)s, pib=%(pib)s, tekuci_racun=%(tekuci_racun)s, opis=%(opis)s WHERE id=%(id)s", komitent)
        db.commit()
        if izmene == 0:
            return flask.jsonify(None), 404
        return flask.jsonify(None), 200
    return "", 403


@app.route("/api/nalozi")
def dobavi_naloge():
    cursor = mysql.get_db().cursor()
    cursor.execute("SELECT * FROM nalog")
    nalozi = cursor.fetchall()
    cursor.execute("SELECT * FROM stavka")
    stavke = cursor.fetchall()
    cursor.execute("SELECT * FROM komitent")
    komitent = cursor.fetchall()
    for nalog in nalozi:
        stavke_temp = []
        for stavka in stavke:
            if nalog["id"] == stavka["nalog_id"]:
                if stavka["komitent_id"] is not None:
                    for k in komitent:
                        if k["id"] == stavka["komitent_id"]:
                            stavka["komitent_naziv"] = k["naziv"]
                            stavke_temp.append(stavka)
                else:
                    stavke_temp.append(stavka)
        nalog["stavke"] = stavke_temp
    if nalozi is None:
        return flask.jsonify(nalozi), 404
    return flask.jsonify(nalozi), 200


@app.route("/api/nalozi/<int:id>")
def dobavi_nalog(id):
    db = mysql.get_db()
    cursor = db.cursor()
    cursor.execute("SELECT * FROM nalog WHERE id=%s", (id, ))
    nalog = cursor.fetchone()
    if nalog is None:
        return flask.jsonify(nalog), 404
    return flask.jsonify(nalog), 200


@app.route("/api/nalozi", methods=["POST"])
def dodavanje_naloga():
    db = mysql.get_db()
    cursor = db.cursor()
    for k in flask.request.json:
        izmene = cursor.execute(
            "INSERT INTO nalog(ime_naloga_sifra, datum_naloga, opis, duguje, potrazuje, saldo, korisnik_id) VALUES(%(ime_naloga_sifra)s, %(datum_naloga)s, %(opis)s, %(duguje)s, %(potrazuje)s, %(saldo)s, %(korisnik_id)s)", k)
    db.commit()
    if izmene == 0:
        return flask.jsonify(None), 500
    return flask.jsonify(None), 201


@app.route("/api/ime_naloga")
def dobavi_ime_naloga():
    cursor = mysql.get_db().cursor()
    cursor.execute("SELECT * FROM ime_naloga")
    ime_naloga = cursor.fetchall()
    if ime_naloga is None:
        return flask.jsonify(ime_naloga), 404
    return flask.jsonify(ime_naloga), 200


@app.route("/api/nalozi/<int:id>", methods=["DELETE"])
def ukoni_nalog(id):
    db = mysql.get_db()
    cursor = db.cursor()
    cursor.execute(
        "SELECT COUNT(id) FROM stavka WHERE nalog_id=%s", (id, ))
    broj = cursor.fetchone()
    if broj["COUNT(id)"] > 0:
        return flask.jsonify(None), 405
    izmene = cursor.execute(
        "DELETE FROM nalog WHERE id=%s and proknjizeno=0", (id, ))
    db.commit()
    if izmene == 0:
        return flask.jsonify(None), 404
    return flask.jsonify(None), 200


# TODO: Nalozi - Izmeni, Storniraj, Stavke


@app.route("/api/nalozi/<int:id>", methods=["PUT"])
def izmena_naloga(id):
    if len(set(flask.session.get("korisnik")["prava_pristupa"]).intersection({1})) > 0:
        db = mysql.get_db()
        cursor = db.cursor()
        cursor.execute("SELECT proknjizeno FROM nalog WHERE id=%s", (id, ))
        p_n = cursor.fetchone()
        if p_n["proknjizeno"] == 1:
            return flask.jsonify(None), 403
        else:
            izmene = cursor.execute(
                "UPDATE nalog SET ime_naloga_sifra=%(ime_naloga_sifra)s, opis=%(opis)s", flask.request.json)
            db.commit()
            if izmene == 0:
                return flask.jsonify(None), 404
            return flask.jsonify(None), 200
    return flask.jsonify(None), 403


@app.route("/api/stavke/<int:id>")
def dobavi_stavke(id):
    cursor = mysql.get_db().cursor()
    cursor.execute("SELECT * FROM stavka WHERE nalog_id=%s", (id, ))
    stavke = cursor.fetchall()
    cursor.execute("SELECT * FROM komitent")
    komitenti = cursor.fetchall()
    for s in stavke:
        for k in komitenti:
            if s["komitent_id"] == k["id"]:
                s["komitent_naziv"] = k["naziv"]

    if stavke is None:
        return flask.jsonify(None), 404
    return flask.jsonify(stavke), 200


@app.route("/api/stavke")
def dobavi_sve_stavke():
    cursor = mysql.get_db().cursor()
    cursor.execute("SELECT * FROM stavka")
    stavke = cursor.fetchall()
    if stavke is None:
        return flask.jsonify(None), 404
    return flask.jsonify(stavke), 200


@app.route("/api/stavke", methods=["POST"])
def dodaj_stavku():
    db = mysql.get_db()
    cursor = db.cursor()
    if flask.request.json["komitent_id"] != "":
        if flask.request.json["komitent_id"] != "":
            izmene = cursor.execute(
                "INSERT INTO stavka(konto_sifra, komitent_id, opis, datum_racuna, datum_valute, potrazuje, duguje, saldo, nalog_id) VALUES(%(konto_sifra)s, %(komitent_id)s, %(opis)s, %(datum_racuna)s, %(datum_valute)s, %(potrazuje)s, %(duguje)s, %(saldo)s,%(nalog_id)s)", flask.request.json)
        else:
            izmene = cursor.execute(
                "INSERT INTO stavka(konto_sifra, komitent_id, opis, datum_racuna, potrazuje, duguje, saldo, nalog_id) VALUES(%(konto_sifra)s, %(komitent_id)s, %(opis)s, %(datum_racuna)s, %(potrazuje)s, %(duguje)s, %(saldo)s,%(nalog_id)s)", flask.request.json)
    else:
        if flask.request.json["komitent_id"] != "":
            izmene = cursor.execute(
                "INSERT INTO stavka(konto_sifra, opis, datum_racuna, datum_valute, potrazuje, duguje, saldo, nalog_id) VALUES(%(konto_sifra)s, %(opis)s, %(datum_racuna)s, %(datum_valute)s, %(potrazuje)s, %(duguje)s, %(saldo)s,%(nalog_id)s)", flask.request.json)
        else:
            izmene = cursor.execute(
                "INSERT INTO stavka(konto_sifra, opis, datum_racuna, potrazuje, duguje, saldo, nalog_id) VALUES(%(konto_sifra)s, %(opis)s, %(datum_racuna)s, %(potrazuje)s, %(duguje)s, %(saldo)s,%(nalog_id)s)", flask.request.json)
    db.commit()
    if izmene == 0:
        return flask.jsonify(None), 500
    return flask.jsonify(None), 201

# FIXME: Ispraviti brisanjestavki iz fakture, DONE


@app.route("/api/stavke/<int:id>", methods=["DELETE"])
def brisanje_stavke(id):
    db = mysql.get_db()
    cursor = db.cursor()
    cursor.execute(
        "SELECT id, duguje, potrazuje, saldo, konto_sifra, nalog_id FROM stavka WHERE id=%s", (id, ))
    stavka = cursor.fetchone()
    if stavka is not None:
        cursor.execute(
            "SELECT * FROM nalog WHERE id=%(nalog_id)s", stavka)
        nalog = cursor.fetchone()
        nalog["duguje"] -= stavka["duguje"]
        nalog["potrazuje"] -= stavka["potrazuje"]
        nalog["saldo"] = nalog["duguje"] - nalog["potrazuje"]
        cursor.execute(
            "UPDATE nalog SET duguje=%(duguje)s, potrazuje=%(potrazuje)s, saldo=%(saldo)s WHERE id=%(id)s", nalog)
        db.commit()
    izmene = cursor.execute("DELETE FROM stavka WHERE id=%s", (id, ))
    db.commit()
    if izmene == 0:
        return flask.jsonify(None), 404
    return flask.jsonify(None), 200


@app.route("/api/nalozi/<int:id>/izmena_cifara", methods=["PUT"])
def izmena_cifara_naloga(id):
    db = mysql.get_db()
    cursor = db.cursor()
    cursor.execute(
        "SELECT duguje, potrazuje, saldo FROM nalog WHERE id=%s", (id, ))
    nalog = cursor.fetchone()

    print(flask.request.json)
    nalog["duguje"] += flask.request.json["duguje"]
    nalog["potrazuje"] += flask.request.json["potrazuje"]
    nalog["saldo"] = nalog["duguje"] - nalog["potrazuje"]
    nalog["id"] = id
    print("nalog:", nalog)
    cursor.execute(
        "UPDATE nalog SET duguje=%(duguje)s, potrazuje=%(potrazuje)s, saldo=%(saldo)s WHERE id=%(id)s", nalog)
    db.commit()
    return flask.jsonify(None), 200


@ app.route("/api/nalozi/proknjizi/<int:id>", methods=["POST"])
def proknjizi_nalog(id):
    db = mysql.get_db()
    cursor = db.cursor()
    datum_knjizenja = time.localtime()
    cursor.execute("SELECT * FROM nalog WHERE id=%s", (id,))
    nalog_za_knjizenje = cursor.fetchone()
    cursor.execute("SELECT * FROM stavka WHERE nalog_id=%s", (id, ))
    stavke_naloga = cursor.fetchall()
    if nalog_za_knjizenje["ime_naloga_sifra"] == "PS":
        for stavka in stavke_naloga:
            cursor.execute(
                "SELECT konto, pocetno_stanje_duguje, pocetno_stanje_potrazuje, pocetno_stanje_saldo FROM kontni_okvir WHERE konto=%s", (stavka["konto_sifra"], ))
            konto = cursor.fetchone()
            konto["pocetno_stanje_duguje"] = stavka["duguje"]
            konto["pocetno_stanje_potrazuje"] = stavka["potrazuje"]
            konto["pocetno_stanje_saldo"] = konto["pocetno_stanje_duguje"] - \
                konto["pocetno_stanje_potrazuje"]
            cursor.execute(
                "UPDATE kontni_okvir SET pocetno_stanje_duguje=%(pocetno_stanje_duguje)s, pocetno_stanje_potrazuje=%(pocetno_stanje_potrazuje)s, pocetno_stanje_saldo=%(pocetno_stanje_saldo)s WHERE konto=%(konto)s", konto)
            db.commit()
        izmene = cursor.execute(
            "UPDATE nalog SET proknjizeno=1, datum_knjizenja=%s WHERE id=%s", (datum_knjizenja, id, ))
        db.commit()
        if izmene == 0:
            return flask.jsonify(None), 404
        return flask.jsonify(None), 200
    else:
        for stavka in stavke_naloga:
            print(stavka)
            cursor.execute(
                "SELECT konto, duguje, potrazuje FROM kontni_okvir WHERE konto=%s", (stavka["konto_sifra"], ))
            konto = cursor.fetchone()
            if konto["duguje"] is None:
                konto["duguje"] = stavka["duguje"]
            else:
                konto["duguje"] += stavka["duguje"]
            if konto["potrazuje"] is None:
                konto["potrazuje"] = stavka["potrazuje"]
            else:
                konto["potrazuje"] += stavka["potrazuje"]
            konto["saldo"] = konto["duguje"] - konto["potrazuje"]
            cursor.execute(
                "UPDATE kontni_okvir SET duguje=%(duguje)s, potrazuje=%(potrazuje)s, saldo=%(saldo)s WHERE konto=%(konto)s", konto)
            db.commit()
        izmene = cursor.execute(
            "UPDATE nalog SET proknjizeno=1, datum_knjizenja=%s WHERE id=%s", (datum_knjizenja, id, ))
        db.commit()
    if izmene == 0:
        return flask.jsonify(None), 404
    return flask.jsonify(None), 200

# TODO: implementirati storniranje naloga koje moze izvrsiti samo administrator
# TODO: implementiraj administratorski panel


@app.route("/korisnici")
def pregled_korisnika():
    return app.send_static_file("administrator/korisnici.html")


@app.route("/api/korisnici/sve")
def dobavi_sve_korisnike():
    if len(set(flask.session.get("korisnik")["prava_pristupa"]).intersection({3})) > 0:
        cursor = mysql.get_db().cursor()
        cursor.execute("SELECT * FROM korisnik")
        korisnici = cursor.fetchall()

        for k in korisnici:
            k["password"] = str(base64.b64decode(k["password"]))[
                2:len(str(base64.b64decode(k["password"]))) - 1]

        if korisnici is None:
            return flask.jsonify(korisnici), 404
        return flask.jsonify(korisnici), 200
    return flask.redirect("/"), 403


@app.route("/api/prava_pristupa/<int:korisnik_id>")
def dobavi_prava_pristupa(korisnik_id):
    if len(set(flask.session.get("korisnik")["prava_pristupa"]).intersection({3})) > 0:
        cursor = mysql.get_db().cursor()
        cursor.execute(
            "SELECT * FROM dodeljeno_pravo WHERE korisnik_id=%s", (korisnik_id, ))
        dodeljena_prava_pristupa = cursor.fetchall()
        cursor.execute("SELECT * FROM pravo_pristupa")
        pravo_pristupa = cursor.fetchall()

        for dodeljena_prava in dodeljena_prava_pristupa:
            for p in pravo_pristupa:
                if dodeljena_prava["pravo_pristupa_id"] == p["id"]:
                    dodeljena_prava["naziv"] = p["naziv"]

        if dodeljena_prava_pristupa is None:
            return flask.jsonify(dodeljena_prava_pristupa), 404
        return flask.jsonify(dodeljena_prava_pristupa), 200
    return flask.redirect("/"), 403


@app.route("/api/prava_pristupa_naziv")
def pp_naziv():
    cursor = mysql.get_db().cursor()
    cursor.execute("SELECT * FROM pravo_pristupa")
    pravo_pristupa = cursor.fetchall()
    if pravo_pristupa is None:
        return flask.jsonify(pravo_pristupa), 404
    return flask.jsonify(pravo_pristupa), 200


@app.route("/api/prava_pristupa/<int:korisnik_id>", methods=["POST"])
def dodavanje_prava(korisnik_id):
    if len(set(flask.session.get("korisnik")["prava_pristupa"]).intersection({3})) > 0:
        db = mysql.get_db()
        cursor = db.cursor()
        pravo = dict(flask.request.json)
        pravo["korisnik_id"] = korisnik_id
        izmene = cursor.execute(
            "INSERT INTO dodeljeno_pravo(korisnik_id, pravo_pristupa_id) VALUES(%(korisnik_id)s, %(pravo_pristupa_id)s)", pravo)
        db.commit()
        if izmene == 0:
            return flask.jsonify(None), 500
        return flask.jsonify(None), 201
    return flask.redirect("/"), 403


@app.route("/api/korisnik/prava/<int:id>", methods=["DELETE"])
def obrisi_pravo(id):
    if len(set(flask.session.get("korisnik")["prava_pristupa"]).intersection({3})) > 0:
        db = mysql.get_db()
        cursor = db.cursor()
        izmene = cursor.execute(
            "DELETE FROM dodeljeno_pravo WHERE id=%s", (id, ))
        db.commit()
        if izmene == 0:
            return flask.jsonify(None), 404
        return flask.jsonify(None), 200
    return flask.redirect("/"), 403


@app.route("/api/korisnici/sve/<int:korisnik_id>")
def dobavi_korisnika_(korisnik_id):
    if len(set(flask.session.get("korisnik")["prava_pristupa"]).intersection({3})) > 0:
        cursor = mysql.get_db().cursor()
        cursor.execute("SELECT * FROM korisnik WHERE id=%s", (korisnik_id, ))
        korisnici = cursor.fetchone()
        korisnici["password"] = str(base64.b64decode(korisnici["password"]))[
            2:len(str(base64.b64decode(korisnici["password"]))) - 1]

        if korisnici is None:
            return flask.jsonify(korisnici), 404
        return flask.jsonify(korisnici), 200
    return flask.redirect("/"), 403


@app.route("/api/korisnici/<int:id>", methods=["DELETE"])
def obrisi_korisnika(id):
    if len(set(flask.session.get("korisnik")["prava_pristupa"]).intersection({3})) > 0:
        db = mysql.get_db()
        cursor = db.cursor()
        cursor.execute(
            "SELECT id FROM dodeljeno_pravo WHERE korisnik_id=%s", (id, ))
        dodeljena_prava = cursor.fetchall()
        if dodeljena_prava is None:
            return flask.jsonify(None), 404

        for d in dodeljena_prava:
            cursor.execute(
                "DELETE FROM dodeljeno_pravo WHERE id=%s", (d["id"], ))
            db.commit()

        izmene = cursor.execute("DELETE FROM korisnik WHERE id=%s", (id, ))
        db.commit()
        if izmene == 0:
            return flask.jsonify(None), 404
        return flask.jsonify(None), 200
    return flask.redirect("/"), 403


@app.route("/api/nalog/storniraj/<int:id>", methods=["POST"])
def storniraj_nalog(id):
    db = mysql.get_db()
    cursor = db.cursor()
    cursor.execute("SELECT * FROM nalog WHERE id=%s", (id, ))
    nalog = cursor.fetchone()
    nalog["opis"] += " - STORNIRANO"
    nalog["datum_knjizenja"] = None
    cursor.execute("SELECT * FROM stavka WHERE nalog_id=%s", (id, ))
    stavke = cursor.fetchall()
    storno_stavke = []
    for s in stavke:
        s["duguje"] = -s["duguje"]
        s["potrazuje"] = -s["potrazuje"]
        s["saldo"] = s["duguje"] - s["potrazuje"]
        s["opis"] += " - STORNIRANO"
        storno_stavke.append(s)
    nalog["saldo"] = nalog["duguje"] - nalog["potrazuje"]
    nalog["duguje"] = 0
    nalog["potrazuje"] = 0
    nalog["saldo"] = 0
    for ss in storno_stavke:
        if nalog["duguje"] == 0:
            nalog["duguje"] = ss["duguje"]
        else:
            nalog["duguje"] += ss["duguje"]
        if nalog["potrazuje"] == 0:
            nalog["potrazuje"] = ss["potrazuje"]
        else:
            nalog["potrazuje"] += ss["potrazuje"]
        nalog["saldo"] = nalog["duguje"] - nalog["potrazuje"]
    cursor.execute(
        "INSERT INTO nalog(datum_naloga, opis, duguje, potrazuje, saldo, ime_naloga_sifra, korisnik_id) VALUES(%(datum_naloga)s, %(opis)s, %(duguje)s, %(potrazuje)s, %(saldo)s, %(ime_naloga_sifra)s, %(korisnik_id)s)", nalog)
    db.commit()
    cursor.execute("SELECT id FROM nalog ORDER BY id DESC LIMIT 1")
    novi_nalog_id = cursor.fetchone()
    for ss in storno_stavke:
        ss["nalog_id"] = novi_nalog_id["id"]
        cursor.execute("INSERT INTO stavka(konto_sifra, duguje, potrazuje, saldo, opis, datum_racuna, datum_valute, nalog_id) VALUES(%(konto_sifra)s, %(duguje)s, %(potrazuje)s, %(saldo)s, %(opis)s, %(datum_racuna)s, %(datum_valute)s, %(nalog_id)s)", ss)
        db.commit()
    return flask.jsonify(None), 200


@app.route("/api/bruto_bilans/zbir")
def zbir_bruto():
    cursor = mysql.get_db().cursor()
    cursor.execute("SELECT * FROM kontni_okvir")
    kontni_okvir = cursor.fetchall()
    for k in kontni_okvir:
        k["pocetno_stanje_duguje"] = 0 if k["pocetno_stanje_duguje"] == None else k["pocetno_stanje_duguje"]
        k["pocetno_stanje_potrazuje"] = 0 if k["pocetno_stanje_potrazuje"] == None else k["pocetno_stanje_potrazuje"]
        k["duguje"] = 0 if k["duguje"] == None else k["duguje"]
        k["potrazuje"] = 0 if k["potrazuje"] == None else k["potrazuje"]
        k["saldo"] = 0 if k["saldo"] == None else k["saldo"]
        k["promet_pocetno_duguje"] = k["pocetno_stanje_duguje"] + k["duguje"]
        k["promet_pocetno_potrazuje"] = k["pocetno_stanje_potrazuje"] + k["potrazuje"]
        if k["konto"] == "0220":
            k["dugovna_saldo"] = k["promet_pocetno_duguje"] - \
                k["promet_pocetno_potrazuje"]
            k["potrazuje_saldo"] = 0
        elif k["konto"] == "0229":
            k["dugovna_saldo"] = 0
            k["potrazuje_saldo"] = k["promet_pocetno_potrazuje"] - \
                k["promet_pocetno_duguje"]
        elif k["konto"] == "2040":
            k["dugovna_saldo"] = k["promet_pocetno_duguje"] - \
                k["promet_pocetno_potrazuje"]
            k["potrazuje_saldo"] = 0
        elif k["konto"] == "3010":
            k["dugovna_saldo"] = 0
            k["potrazuje_saldo"] = k["promet_pocetno_potrazuje"] - \
                k["promet_pocetno_duguje"]
        elif k["konto"] == "3400":
            k["dugovna_saldo"] = 0
            k["potrazuje_saldo"] = k["promet_pocetno_potrazuje"] - \
                k["promet_pocetno_duguje"]
        elif k["konto"] == "4350":
            k["dugovna_saldo"] = 0
            k["potrazuje_saldo"] = k["promet_pocetno_potrazuje"] - \
                k["promet_pocetno_duguje"]
        elif k["konto"] == "6140":
            k["dugovna_saldo"] = 0
            k["potrazuje_saldo"] = k["promet_pocetno_potrazuje"] - \
                k["promet_pocetno_duguje"]
        elif k["konto"] == "2410":
            k["dugovna_saldo"] = k["promet_pocetno_duguje"] - \
                k["promet_pocetno_potrazuje"]
            k["potrazuje_saldo"] = 0
        elif k["konto"] == "5511":
            k["dugovna_saldo"] = k["promet_pocetno_duguje"] - \
                k["promet_pocetno_potrazuje"]
            k["potrazuje_saldo"] = 0
        elif k["konto"] == "5310":
            k["dugovna_saldo"] = k["promet_pocetno_duguje"] - \
                k["promet_pocetno_potrazuje"]
            k["potrazuje_saldo"] = 0
        else:
            k["dugovna_saldo"] = k["promet_pocetno_duguje"] - \
                k["promet_pocetno_potrazuje"]
            k["potrazuje_saldo"] = k["promet_pocetno_potrazuje"] - \
                k["promet_pocetno_duguje"]
    temp = {
        "zbir_pocetno_dugovna": 0,
        "zbir_pocetno_potrazna": 0,
        "zbir_tekuci_dugovna": 0,
        "zbir_tekuci_potrazna": 0,
        "zbir_tekuci_pocetno_dugovna": 0,
        "zbir_tekuci_pocetno_potrazna": 0,
        "zbir_saldo_dugovna": 0,
        "zbir_saldo_potrazna": 0
    }
    for k in kontni_okvir:
        temp["zbir_pocetno_dugovna"] += k["pocetno_stanje_duguje"]
        temp["zbir_pocetno_potrazna"] += k["pocetno_stanje_potrazuje"]
        temp["zbir_tekuci_dugovna"] += k["duguje"]
        temp["zbir_tekuci_potrazna"] += k["potrazuje"]
        temp["zbir_tekuci_pocetno_dugovna"] += k["promet_pocetno_duguje"]
        temp["zbir_tekuci_pocetno_potrazna"] += k["promet_pocetno_potrazuje"]
        temp["zbir_saldo_dugovna"] += k["dugovna_saldo"]
        temp["zbir_saldo_potrazna"] += k["potrazuje_saldo"]
    if kontni_okvir is None:
        return flask.jsonify(temp), 404
    return flask.jsonify(temp), 200


@app.route("/api/glavna_knjiga")
def dobavi_naloge_glavna_knjiga():
    cursor = mysql.get_db().cursor()
    cursor.execute("SELECT * FROM nalog WHERE proknjizeno=1")
    nalozi = cursor.fetchall()
    cursor.execute("SELECT * FROM stavka")
    stavke = cursor.fetchall()
    cursor.execute("SELECT * FROM komitent")
    komitent = cursor.fetchall()
    for nalog in nalozi:
        stavke_temp = []
        for stavka in stavke:
            if nalog["id"] == stavka["nalog_id"]:
                if stavka["komitent_id"] is not None:
                    for k in komitent:
                        if k["id"] == stavka["komitent_id"]:
                            stavka["komitent_naziv"] = k["naziv"]
                            stavke_temp.append(stavka)
                else:
                    stavke_temp.append(stavka)
        nalog["stavke"] = stavke_temp
    if nalozi is None:
        return flask.jsonify(nalozi), 404
    return flask.jsonify(nalozi), 200


@app.route("/api/korisnici/novi_korisnik", methods=["POST"])
def dodaj_novog_korisnika():
    if len(set(flask.session.get("korisnik")["prava_pristupa"]).intersection({3})) > 0:
        promene = 0
        db = mysql.get_db()
        cursor = db.cursor()
        for k in flask.request.json:
            username_input = k['username']
            password_input = k['password'].encode('UTF-8')
            encoded_password_input = base64.b64encode(password_input)
            promene += cursor.execute(
                "INSERT INTO korisnik(username, password, email) VALUES(%s, %s, %s)", (username_input, encoded_password_input, k["email"]))
            db.commit()
        if promene == 0:
            return flask.jsonify(None), 500
        return flask.jsonify(None), 201
    return "", 403


@app.route("/api/glavna_knjiga/zbir")
def zbir_bruto_bilans():
    cursor = mysql.get_db().cursor()
    cursor.execute("SELECT * FROM nalog WHERE proknjizeno=1")
    nalozi = cursor.fetchall()
    zbir = {
        "duguje": 0,
        "potrazuje": 0,
        "saldo": 0
    }
    for n in nalozi:
        zbir["duguje"] += n["duguje"]
        zbir["potrazuje"] += n["potrazuje"]
    zbir["saldo"] = zbir["duguje"] - zbir["potrazuje"]
    return flask.jsonify(zbir), 200
